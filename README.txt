***********
* README: *
***********

DESCRIPTION:
------------
If you use ubercart and Ubercart Webform Checkout Pane you will be able to use webform as part of checkout process. However, if it is a returned user who already fill out the webform, they still have to refill it.
This module is use to re-populate the already submitted data for the returned users. Users can then keep it as it is or modify it and resubmit. The new data will be saved and pre-populate next time they come back.

Module Dependencies
This module requires Ubercart Webform Checkout Pane which then requires other modules to work.

Future work
The module will continue develop to integrate between Ubercart Webform Checkout Pane and Profiles.

INSTALLATION:
-------------
1. Place the entire email directory into your Drupal sites/all/modules/
   directory.

2. Enable the email module by navigating to:

     administer > modules

Author:
-------
Truyen Le
truyenle@drupal.org
truyenlv2002@yahoo.com

